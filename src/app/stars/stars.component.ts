import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-stars',
    templateUrl: './stars.component.html',
    styleUrls: ['./stars.component.css']
})
export class StarsComponent implements OnInit, OnChanges {
    @Input()
    private rating: number = 0;

    @Output('ratingChange')
    ratingChange: EventEmitter<number> = new EventEmitter();

    @Input()
    private readonly: boolean = true;

    private stars: boolean[];

    constructor() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        this.stars = [];
        for (let i = 1; i <= 5; i++) {
            this.stars.push(i > this.rating);
        }        
    }

    clickStar(index:number) {
        if (this.readonly) {
            return;
        }
        this.rating = index + 1;
        this.ratingChange.emit(this.rating);
    }

}
