import * as express from 'express';

import {Server} from 'ws';

const subscription = new Map<any, number[]>();

const wsServer = new Server({port: 8085});
wsServer.on('connection', websocket => {
	// websocket.send('这是服务器主动推送的消息');
	websocket.on('message', message => {
		let messageObj = JSON.parse(message);
		console.log(messageObj);
		let productIds = subscription.get(websocket) || [];
		subscription.set(websocket, [...productIds, messageObj.productId]);
	});
});

const currentBids = new Map<number, number>();

setInterval(() => {
	products.forEach(p => {
		let currentBid = currentBids.get(p.id) || p.price;
		let newBid = currentBid + Math.random() * 5;
		currentBids.set(p.id, newBid);
	});

	subscription.forEach((productIds: number[], ws) => {
		if (ws.readyState === 1) {
			let newBids = productIds.map(pid => ({
				productId: pid,
				bid: currentBids.get(pid)
			}));
			ws.send(JSON.stringify(newBids));
		} else {
			subscription.delete(ws);
		}

	});

}, 2000);

export class Comment {
  constructor(
    public id: number,
    public productId: number,
    public timestamp: string,
    public user: string,
    public rating: number,
    public content: string
  ) {

  }
}

  const comments: Comment[] = [
    new Comment(1, 1, '2017-02-02 22:22:22', 'zhangsan', 3, '东西不错'),
    new Comment(2, 1, '2017-11-02 22:22:22', 'wangwu', 2, '东西不错'),
    new Comment(3, 1, '2017-03-02 22:22:22', 'wanger', 4, '东西不错'),
    new Comment(4, 1, '2017-08-02 22:22:22', 'lisi', 5, '东西不错'),
    new Comment(1, 2, '2017-02-02 22:22:22', 'zhangsan1', 3, '东西不错'),
    new Comment(2, 2, '2017-02-02 22:22:22', 'wangsan', 1, '东西不错'),
    new Comment(3, 2, '2017-02-02 22:22:22', 'sunl', 3, '东西不错'),
    new Comment(4, 2, '2017-02-02 22:22:22', 'zhangsan', 2, '东西不错')
  ];

export class Product {
  constructor(
      public id: number,
      public title: string,
      public price: number,
      public rating: number,
      public desc: string,
      public categories: string[]) {
  }
}

const products: Product[] = [
  new Product(1, "第1个商品", 5.29, 4.5, "测试实例1", ["电子产品", "商务"]),
  new Product(2, "第2个商品", 1.59, 3.5, "测试实例2", ["电子产品", "商务"]),
  new Product(3, "第3个商品", 2.29, 3.5, "测试实例3", ["电子产品", "商务"]),
  new Product(4, "第4个商品", 16.99, 2.5, "测试实例4", ["电子产品", "商务"]),
  new Product(5, "第5个商品", 122.99, 1.5, "测试实例5", ["电子产品", "商务"])
];

const app = express();

let index:number = 0;

app.get('/', (req, res) => {
	res.send('hello express');
});

app.get('/api/products', (req, res) => {
	let result = products;
	let params = req.query;
	if (params.title) {
		result = result.filter(p => p.title.indexOf(params.title) !== -1);
	}

	if (params.price && result.length > 0) {
		result  = result.filter(p => p.price <= parseInt(params.price));
	}

	if (params.category !== '-1' && result.length > 0) {
		result  = result.filter(p => p.categories.indexOf(params.category) !== -1);
	}

	res.json(result);
});

app.get('/api/product/:id', (req, res) => {
	res.json(products.find((product) => product.id == req.params.id));
});

app.get('/api/product/:id/comments', (req, res) => {
	res.json(comments.filter((comment: Comment) => comment.productId == req.params.id));
});

const server = app.listen(8000, 'localhost', () => {
	console.log('服务器已经启动，地址localhost8000');
});