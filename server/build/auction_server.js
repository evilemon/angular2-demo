"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var ws_1 = require("ws");
var subscription = new Map();
var wsServer = new ws_1.Server({ port: 8085 });
wsServer.on('connection', function (websocket) {
    // websocket.send('这是服务器主动推送的消息');
    websocket.on('message', function (message) {
        var messageObj = JSON.parse(message);
        console.log(messageObj);
        var productIds = subscription.get(websocket) || [];
        subscription.set(websocket, productIds.concat([messageObj.productId]));
    });
});
var currentBids = new Map();
setInterval(function () {
    products.forEach(function (p) {
        var currentBid = currentBids.get(p.id) || p.price;
        var newBid = currentBid + Math.random() * 5;
        currentBids.set(p.id, newBid);
    });
    subscription.forEach(function (productIds, ws) {
        if (ws.readyState === 1) {
            var newBids = productIds.map(function (pid) { return ({
                productId: pid,
                bid: currentBids.get(pid)
            }); });
            ws.send(JSON.stringify(newBids));
        }
        else {
            subscription.delete(ws);
        }
    });
}, 2000);
var Comment = /** @class */ (function () {
    function Comment(id, productId, timestamp, user, rating, content) {
        this.id = id;
        this.productId = productId;
        this.timestamp = timestamp;
        this.user = user;
        this.rating = rating;
        this.content = content;
    }
    return Comment;
}());
exports.Comment = Comment;
var comments = [
    new Comment(1, 1, '2017-02-02 22:22:22', 'zhangsan', 3, '东西不错'),
    new Comment(2, 1, '2017-11-02 22:22:22', 'wangwu', 2, '东西不错'),
    new Comment(3, 1, '2017-03-02 22:22:22', 'wanger', 4, '东西不错'),
    new Comment(4, 1, '2017-08-02 22:22:22', 'lisi', 5, '东西不错'),
    new Comment(1, 2, '2017-02-02 22:22:22', 'zhangsan1', 3, '东西不错'),
    new Comment(2, 2, '2017-02-02 22:22:22', 'wangsan', 1, '东西不错'),
    new Comment(3, 2, '2017-02-02 22:22:22', 'sunl', 3, '东西不错'),
    new Comment(4, 2, '2017-02-02 22:22:22', 'zhangsan', 2, '东西不错')
];
var Product = /** @class */ (function () {
    function Product(id, title, price, rating, desc, categories) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.rating = rating;
        this.desc = desc;
        this.categories = categories;
    }
    return Product;
}());
exports.Product = Product;
var products = [
    new Product(1, "第1个商品", 5.29, 4.5, "测试实例1", ["电子产品", "商务"]),
    new Product(2, "第2个商品", 1.59, 3.5, "测试实例2", ["电子产品", "商务"]),
    new Product(3, "第3个商品", 2.29, 3.5, "测试实例3", ["电子产品", "商务"]),
    new Product(4, "第4个商品", 16.99, 2.5, "测试实例4", ["电子产品", "商务"]),
    new Product(5, "第5个商品", 122.99, 1.5, "测试实例5", ["电子产品", "商务"])
];
var app = express();
var index = 0;
app.get('/', function (req, res) {
    res.send('hello express');
});
app.get('/api/products', function (req, res) {
    var result = products;
    var params = req.query;
    if (params.title) {
        result = result.filter(function (p) { return p.title.indexOf(params.title) !== -1; });
    }
    if (params.price && result.length > 0) {
        result = result.filter(function (p) { return p.price <= parseInt(params.price); });
    }
    if (params.category !== '-1' && result.length > 0) {
        result = result.filter(function (p) { return p.categories.indexOf(params.category) !== -1; });
    }
    res.json(result);
});
app.get('/api/product/:id', function (req, res) {
    res.json(products.find(function (product) { return product.id == req.params.id; }));
});
app.get('/api/product/:id/comments', function (req, res) {
    res.json(comments.filter(function (comment) { return comment.productId == req.params.id; }));
});
var server = app.listen(8000, 'localhost', function () {
    console.log('服务器已经启动，地址localhost8000');
});
